DROP TABLE IF EXISTS `paukščių_rūšys`;
		
CREATE TABLE `paukščių_rūšys` (
  `ID` INTEGER NOT NULL AUTO_INCREMENT,
  `TYPE` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`ID`)
) COMMENT 'Paukščių rūšių lentelė';

DROP TABLE IF EXISTS `stebėti_paukščiai`;
		
CREATE TABLE `stebėti_paukščiai` (
  `ID` INTEGER NOT NULL AUTO_INCREMENT,
  `TYPEID` INTEGER NOT NULL,
  `BIRDNAME` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`ID`)
) COMMENT 'Stebėtų paukščių lentelė';

DROP TABLE IF EXISTS `stebėjimų_koordinatės`;
		
CREATE TABLE `stebėjimų_koordinatės` (
  `ID` INTEGER NULL AUTO_INCREMENT,
  `BIRDID` INTEGER NOT NULL,
  `COORDINATES` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`ID`)
) COMMENT 'Stebėjimų koordinačių lentelė';

ALTER TABLE `stebėti_paukščiai` ADD FOREIGN KEY (TYPEID) REFERENCES `paukščių_rūšys` (`ID`);
ALTER TABLE `stebėjimų_koordinatės` ADD FOREIGN KEY (BIRDID) REFERENCES `stebėti_paukščiai` (`ID`);