document.addEventListener("DOMContentLoaded", function(event){
  GenerateList();
  generateAllWatchings();
  generateDeleteWatchingsList();
})

var ListOfWatchings = [[0, "Paukštis 1", "coorindates1"], [1, "Paukštis 2", "coorindates2"],
    [2, "Paukštis 3", "coorindates3"], [3, "Paukštis 4", "coorindates4"], [4, "Paukštis 5", "coorindates5"],
    [5, "Paukštis 6", "coorindates6"], [6, "Paukštis 7", "coorindates7"], [7, "Paukštis 8", "coorindates8"]];

var ListOfBirds = [[0, "Gegutė"], [1, "Varna"], [2, "Erelis"], [3, "Balandis"], [4, "Gandras"], [5, "Apuokas"]];

function GenerateList() {
    var myDiv = document.getElementById("birdsList");
    for (var i = 0; i < ListOfBirds.length; i++) {
        var option = document.createElement("option");
        option.value = ListOfBirds[i][1];
        option.text = ListOfBirds[i][1];
        myDiv.appendChild(option);
    }
}

function generateAllWatchings () {
    var myDiv = document.getElementById("watchings_List");
    for (var i = 0; i < ListOfWatchings.length; i++) {
        var option = document.createElement("option");
        option.value = ListOfWatchings[i][1];
        option.text = ListOfWatchings[i][1];
        myDiv.appendChild(option);
    }
}

function generateDeleteWatchingsList () {
    var myDiv = document.getElementById("delete_Watchings_List");
    for (var i = 0; i < ListOfWatchings.length; i++) {
        var option = document.createElement("option");
        option.value = ListOfWatchings[i][1];
        option.text = ListOfWatchings[i][1];
        myDiv.appendChild(option);
    }
}

function getBirdID (index) {
    bird_api.RemoveBird(ListOfBirds[index - 1][0]);
}

function getWatchingID (index) {
    bird_api.GetAllWatchings(ListOfWatchings[index - 1][0]);
}

function getDeleteWatchingID (index) {
    bird_api.RemoveWatching(ListOfWatchings[index - 1][0]);
}