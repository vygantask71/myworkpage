var newBird = document.getElementById("new_Bird");
var deleteBird = document.getElementById("delete_Bird");
var watchingsList = document.getElementById("watchings_list");
var newWatching = document.getElementById("new_Watching");
var deleteWatching = document.getElementById("delete_Watching");

function hideDisplay() {
    var nodes = document.getElementById('box_Content').children;
        //.getElementsByTagName("div");
    for (var i = 0 ; i < nodes.length; i++) {
        nodes[i].style.display = 'none';
    }
}

function clearFocus() {
    var nodes = document.getElementById('nav_List').children;
    for (var i = 0 ; i < nodes.length; i++) {
        nodes[i].removeAttribute('class');
    }
}

newBird.onclick = function() {
    hideDisplay();
    clearFocus();
    document.getElementById("new_Bird_Content").style.display = 'block';
    document.getElementById("new_Bird").className = 'focused';
};
deleteBird.onclick = function() {
    hideDisplay();
    clearFocus();
    document.getElementById("delete_Bird_Content").style.display = 'block';
    document.getElementById("delete_Bird").className = 'focused';
};
watchingsList.onclick = function() {
    hideDisplay();
    clearFocus();
    document.getElementById("watchings_list_Content").style.display = 'block';
    document.getElementById("watchings_list").className = 'focused';
};
newWatching.onclick = function() {
    hideDisplay();
    clearFocus();
    document.getElementById("new_Watching_Content").style.display = 'block';
    document.getElementById("new_Watching").className = 'focused';
};
deleteWatching.onclick = function() {
    hideDisplay();
    clearFocus();
    document.getElementById("delete_Watching_Content").style.display = 'block';
    document.getElementById("delete_Watching").className = 'focused';
};