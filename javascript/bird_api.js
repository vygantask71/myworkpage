var bird_api = {};

bird_api.AddBird = function(value) {
    var data = {
        Name: value,
        Command: "addNewBird"
    };
    alert(JSON.stringify(data, null, 4));
}

bird_api.RemoveBird = function(value)
{
    var data = {
        BirdID: value,
        Command: "deleteBird"
    };
     alert(JSON.stringify(data, null, 4));
}

bird_api.GetAllWatchings = function(value) {
    var data = {
        WatchingID: value,
        Command: "receiveDataAboutWatching"
    };
    alert(JSON.stringify(data, null, 4));
}

bird_api.RemoveWatching = function(value) {
    var data = {
        WatchingID: value,
        Command: "deleteWatching"
    };
    alert(JSON.stringify(data, null, 4));
}

bird_api.AddNewWatching = function(birdName, coordinates) {
    var data = {
        Name: birdName,
        Coordinates: coordinates,
        Command: "addNewWatching"
    };
    alert(JSON.stringify(data, null, 4));
}

window.bird_api = bird_api;